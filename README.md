# monogame-extended-animation-tutorial

A quick tutorial on how to do animation with SpriteFactory and Monogame Extended

---

## Creating the animated sprite with SpriteFactory

1. Get [SpriteFactory](https://github.com/craftworkgames/SpriteFactory)
1. Load your spritesheet in SpriteFactory. I'm using a spritesheet from the [Ninja Adventure Asset Pack](https://pixel-boy.itch.io/ninja-adventure-asset-pack) for this tutorial.
1. Import the spritesheet into SpriteFactory and set the correct tile size
1. Select the sprites that make up a full animation, click + Add and name it. For the brevity of this tutorial I'll just add: `walkUp`, `walkDown`, `walkLeft`, `walkRight`, `idleUp`, `idleDown`, `idleLeft`, `idleRight`.
1. Save the finished product as a *.sf file. 

## Animating the Sprite in MonoGame with MonoGame Extended

1. Create a new Project: 
    ```csharp
    dotnet new mgdesktopgl -o animation-game
    ```
1. Add `Monogame.Extended` via nuget 
    ```csharp
    dotnet add package MonoGame.Extended
    ```
1. Add the spritesheet `*.png` to your project using the MonoGame Content Builder (NOTE: The `*.png` **not** the `*.sf`!)
1. Add the `*.sf` file to the `Content` folder. We don't have to use the Content pipeline for this, but we have to edit the settings of this file in the Solution Explorer. Set `Copy to Output Directorty` to `Copy if newer`. 
    ![Copy if Newer settings](copy-if-newer.png)
    1. *Alternatively* we can add the folowing lines to our `*.csproj` file:
        ```csharp
        <ItemGroup>
            <None Update="Content\monk-spritesheet.sf">
                <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>
            </None>
        </ItemGroup>
        ```
1. We are going to add two member variables, one is of type `Vector2` and one is of type `AnimatedSprite`. For this we need to include some libraries:
    ```csharp
    using Microsoft.Xna.Framework;
    using MonoGame.Extended.Sprites;
    ```
    And then add member variables:
    ```csharp
    private AnimatedSprite _sprite;
    private Vector2 _position;
    ```
1. In `LoadContent` we are going to load the sprite. Before we can do that we need to include the `MonoGame.Extended.Serialization` library so we have access to `JsonContentLoader`, to read our `*.sf` file (which is using the JSON format). We also have to load `MonoGame.Extended.Content` so we get access to the MonoGame.Extended's version of `Content.Load` that accepts a custom loader.
    ```chsarp
    using MonoGame.Extended.Content;
    using MonoGame.Extended.Serialization;
    ```

    Then add the following code to the `LoadContent` method:
    ```csharp
    var spriteSheet = Content.Load<SpriteSheet>("monk-spritesheet.sf", new JsonContentLoader());
    var sprite = new AnimatedSprite(spriteSheet);

    sprite.Play("idleDown");
    _position = new Vector2(100, 100);
    _sprite = sprite;
    ```
1. Now that we have our sprite, we can draw it. Add the following lines to the `Draw` method:
    ```csharp
    _spriteBatch.Begin();
    _spriteBatch.Draw(_sprite, _position);
    _spriteBatch.End();
    ```
1. We can run the game now (<kbd>F5</kbd>), but it won't be animated yet. Let's work on that next.
1. We want to change the animation based on a few criteria:
    - If the player is pressing <kbd>W</kbd> we want to show the `movingLeft` animation
    - If the player is **not** pressing <kbd>W</kbd> but the player's last movement was the the left, we want to show the `idleLeft` animation
    - The same for all other directions.
1. This is default movement stuff in Monogame so I'll go over this rather quickly. There are great tutorials out there for movement. Add the following member variables:
    ```csharp
        private KeyboardState _keyboardState, _previousKeyboardState;
        private string animation = "idleDown"; // The starting animation
    ```
1. In the `Update` method we want to set the keyboard states and set some other values. Add the following lines to the top of the method:
    ```csharp
    _previousKeyboardState = _keyboardState;
    _keyboardState = Keyboard.GetState();
    var deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;
    var walkSpeed = deltaSeconds * 128;
    ```
1. Here we check for actual input and then update the animation string and the player's position:   
    ```csharp
    bool isMoving = false;

    if (_keyboardState.IsKeyDown(Keys.W)) // Currently walking up
    {
        animation = "walkUp";
        _position.Y -= walkSpeed;
        isMoving = true;
    }

    if (_keyboardState.IsKeyDown(Keys.S))
    {
        animation = "walkDown";
        _position.Y += walkSpeed;
        isMoving = true;
    }

    if (_keyboardState.IsKeyDown(Keys.A))
    {
        animation = "walkLeft";
        _position.X -= walkSpeed;
        isMoving = true;
    }

    if (_keyboardState.IsKeyDown(Keys.D))
    {
        animation = "walkRight";
        _position.X += walkSpeed;
        isMoving = true;
    }

    if (!isMoving)
    {
        if (_previousKeyboardState.IsKeyDown(Keys.W)) // Idle now, but last movement was up
            animation = "idleUp";
        else if (_previousKeyboardState.IsKeyDown(Keys.S))
            animation = "idleDown";
        else if (_previousKeyboardState.IsKeyDown(Keys.A))
            animation = "idleLeft";
        else if (_previousKeyboardState.IsKeyDown(Keys.D))
            animation = "idleRight";
    }
    ```
1. And now we actually want to update the animation of the sprite:
    ```csharp
    _sprite.Play(animation);
    _sprite.Update(deltaSeconds);
    ```
1. And that's all there is to it! All the 'magic' happens by `AnimatedSprite`.