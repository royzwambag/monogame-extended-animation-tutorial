﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Sprites;
using MonoGame.Extended.Serialization;
using MonoGame.Extended.Content;

namespace animation_game
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private AnimatedSprite _sprite;
        private Vector2 _position;
        private KeyboardState _keyboardState, _previousKeyboardState;
        private string animation = "idleDown"; // The starting animation

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            var spriteSheet = Content.Load<SpriteSheet>("monk-spritesheet.sf", new JsonContentLoader());
            var sprite = new AnimatedSprite(spriteSheet);

            sprite.Play("idleDown");
            _position = new Vector2(100, 100);
            _sprite = sprite;
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            _previousKeyboardState = _keyboardState;
            _keyboardState = Keyboard.GetState();

            var deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;
            var walkSpeed = deltaSeconds * 128;

            bool isMoving = false;

            if (_keyboardState.IsKeyDown(Keys.W)) // Currently walking up
            {
                animation = "walkUp";
                _position.Y -= walkSpeed;
                isMoving = true;
            }

            if (_keyboardState.IsKeyDown(Keys.S))
            {
                animation = "walkDown";
                _position.Y += walkSpeed;
                isMoving = true;
            }

            if (_keyboardState.IsKeyDown(Keys.A))
            {
                animation = "walkLeft";
                _position.X -= walkSpeed;
                isMoving = true;
            }

            if (_keyboardState.IsKeyDown(Keys.D))
            {
                animation = "walkRight";
                _position.X += walkSpeed;
                isMoving = true;
            }

            if (!isMoving)
            {
                if (_previousKeyboardState.IsKeyDown(Keys.W)) // Idle now, but last movement was up
                    animation = "idleUp";
                else if (_previousKeyboardState.IsKeyDown(Keys.S))
                    animation = "idleDown";
                else if (_previousKeyboardState.IsKeyDown(Keys.A))
                    animation = "idleLeft";
                else if (_previousKeyboardState.IsKeyDown(Keys.D))
                    animation = "idleRight";
            }

            _sprite.Play(animation);
            _sprite.Update(deltaSeconds);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            _spriteBatch.Draw(_sprite, _position);
            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
